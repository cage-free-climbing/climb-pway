import React from 'react';

import AuthUserContext from './AuthUserContext';
import { firebase } from '../../firebase';
import LogRocket from 'logrocket';

const withAuthentication = (Component) =>
  class WithAuthentication extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        authUser: null,
      };
    }

    componentDidMount() {
      firebase.auth.onAuthStateChanged(authUser => {
        authUser
          ? this.setState(() => ({ authUser }))
          : this.setState(() => ({ authUser: null }));
      });      
      firebase.auth.onAuthStateChanged(authUser => {
        if (authUser){
          LogRocket.identify(authUser.uid, {
            name: authUser.name,
            email: authUser.email,
          });
        }
      });
    }

    render() {
      const { authUser } = this.state;

      return (
        <AuthUserContext.Provider value={authUser}>
          <Component />
        </AuthUserContext.Provider>
      );
    }
  }
  
LogRocket.init('lywvnk/climb-pway');

export default withAuthentication;