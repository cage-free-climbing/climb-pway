import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

//keys and endpoints config

var prodConfig = {
  apiKey: process.env.REACT_APP_PROD_FIREBASE_APIKEY,
  authDomain: process.env.REACT_APP_PROD_FIREBASE_AUTHDOMAIN,
  databaseURL: process.env.REACT_APP_PROD_FIREBASE_DATABASEURL,
  projectId: process.env.REACT_APP_PROD_FIREBASE_PROJECTID,
  storageBucket: process.env.REACT_APP_PROD_FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_PROD_FIREBASE_MESSAGINGSENDERID,
};

const devConfig = {
  apiKey: process.env.REACT_APP_DEV_FIREBASE_APIKEY,
  authDomain: process.env.REACT_APP_DEV_FIREBASE_AUTHDOMAIN,
  databaseURL: process.env.REACT_APP_DEV_FIREBASE_DATABASEURL,
  projectId: process.env.REACT_APP_DEV_FIREBASE_PROJECTID,
  storageBucket: process.env.REACT_APP_DEV_FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_DEV_FIREBASE_MESSAGINGSENDERID,
};

const config = process.env.NODE_ENV === 'production'
? prodConfig
: devConfig;

//if not init then init
if (!firebase.apps.length) {
firebase.initializeApp(config);
}

//init auth
const auth = firebase.auth();

//init db
const db = firebase.database();

export {
  db,
  auth,
};