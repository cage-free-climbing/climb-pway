//main
import React, { Component } from "react";
import { compose } from "recompose";

//styles
import { withStyles } from "@material-ui/core/styles";

//router
import AuthUserContext from "../authlogic/AuthUserContext";
import { withRouter } from "react-router-dom";

//auth
import { auth, db } from "../../firebase";

//components
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { injectStripe } from "react-stripe-elements";
import CardSection from "./CardSection";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    textTransform: "none",
    fontWeight: 600
  },
  containerTop: {
    marginTop: theme.spacing.unit * 3
  },
  containerBottom: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3
  },
  buttonLeft: {
    margin: -theme.spacing.unit * 3
  },
  buttonright: {
    textTransform: "none",
    fontWeight: 600
  }
});

class PaymentForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      canMakePayment: true
    };
  }

  handleSubmit = ev => {
    ev.preventDefault();
    this.props.stripe
      .createToken({ name: this.props.authUser.email })
      .then(({ token }) => {
        console.log("createToken", token);
        db.doCreatePaymentSource(token, this.props.authUser.uid)
          .then(() => {
            console.log("doCreatePaymentSource TOKEN");
            console.log(token);
            db.doCreatePaymentCharge(token, this.props.authUser.uid).then(
              () => {
                console.log("doCreatePaymentCharge TOKEN");
                console.log(token);
                this.setState({ canMakePayment: false });
                // history.push(routes.HOME);
              }
            );
          })
          .catch(error => {
            console.log("do create payment source ERROR");
            console.log(error);
          });
      });
  };

  render() {
    const { classes } = this.props;
    return this.state.canMakePayment ? (
      <form onSubmit={this.handleSubmit}>
        <CardSection />
        <Button
          className={classes.button}
          type="submit"
          color="primary"
          variant="contained"
        >
          Confirm Order
        </Button>
      </form>
    ) : (
      <Typography className={classes.display} variant="title" gutterBottom>
        Thank you for your support!
      </Typography>
    );
  }
}

export default compose(
  withRouter,
  injectStripe,
  withStyles(styles)
)(PaymentForm);
