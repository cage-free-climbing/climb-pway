//main
import React, { Component } from 'react';
import { compose } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import IconButton from '@material-ui/core/IconButton';
import PersonPinIcon from '@material-ui/icons/PersonPin';

class User extends Component {
  render() {
    const {classes, size = 24, onClick} = this.props;
    return (
      <IconButton className={classes.backButton} color='primary' aria-label="Back" onClick={this.goBack}>
        <PersonPinIcon />
      </IconButton>
    );
  }
}

export default compose(
  withStyles(withStyles),
 ) (User);