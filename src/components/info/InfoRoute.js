//main
import React, { Component } from "react";
import { compose } from "recompose";

//router
import withAuthorization from "../authlogic/withAuthorization";

//styles
import { withStyles } from "@material-ui/core/styles";

//components
import StarRating from "../common/StarRating";
import ImageList from "../common/ImageList";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
  root: {},
  container: {
    paddingTop: theme.spacing.unit * 9
  },
  item: {
    paddingBottom: theme.spacing.unit * 1
  },
  caption: {
    // fontWeight: 600
  },
  paper: {
    padding: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 1,
    boxShadow: "0 0 30px 6px rgba(31,51,73,.1)!important"
  }
});

class RouteInfo extends Component {
  render() {
    const { classes, data } = this.props;
    return (
      <div className={classes.root}>
        <Grid
          className={classes.container}
          container
          alignItems="stretch"
          direction="column"
          justify="flex-start"
        >
          <Grid className={classes.item} item>
            <Paper className={classes.paper}>
              <Typography
                className={classes.caption}
                variant="h6"
                gutterBottom
                color="textPrimary"
              >
                General
              </Typography>
              <Grid
                className={classes.containerSmall}
                container
                alignItems="flex-start"
                direction="row"
                justify="space-between"
              >
                <Grid className={classes.itemSmall} item>
                  <Typography variant="body1" gutterBottom>
                    V{data.route.grade}
                  </Typography>
                </Grid>
                <Grid className={classes.itemSmall} item>
                  <StarRating rating={data.route.rating} />
                </Grid>
                <Grid className={classes.itemSmall} item>
                  <Typography variant="body1" gutterBottom>
                    {data.route.type}
                  </Typography>
                </Grid>
                <Grid className={classes.itemSmall} item>
                  <Typography variant="body1" gutterBottom>
                    {data.route.fadate}
                  </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid className={classes.item} item>
            <Paper className={classes.paper}>
              <Typography
                className={classes.caption}
                variant="h6"
                gutterBottom
                color="textPrimary"
              >
                Media
              </Typography>
              <ImageList items={data.route.mediaCollection.items} />
            </Paper>
          </Grid>
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.caption}
                variant="h6"
                gutterBottom
                color="textPrimary"
              >
                Description
              </Typography>
              <Typography variant="body1" gutterBottom>
                {data.route.description}
              </Typography>
            </Paper>
          </Grid>
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.caption}
                variant="h6"
                gutterBottom
                color="textPrimary"
              >
                Access
              </Typography>
              <Typography variant="body1" gutterBottom>
                {data.route.description}
              </Typography>
            </Paper>
          </Grid>
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.caption}
                variant="h6"
                gutterBottom
                color="textPrimary"
              >
                Protection
              </Typography>
              <Typography variant="body1" gutterBottom>
                {data.route.description}
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const authCondition = authUser => !!authUser;

export default compose(
  withAuthorization(authCondition),
  withStyles(styles)
)(RouteInfo);
