//main
import React from "react";

//theme
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import "typeface-lato";

//components
import CssBaseline from "@material-ui/core/CssBaseline";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#FF1654"
    },
    secondary: {
      main: "#FF1654"
    }
  },
  typography: {
    useNextVariants: true,
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Lato"',
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    h4: {
      // fontWeight: 600,
      // color: "rgba(0, 0, 0, 0.87)"
    },
    caption: {
      fontWeight: 600
      // color: "rgba(0, 0, 0, 0.87)"
    },
    h6: {
      fontWeight: 600
    }
  },
  overrides: {
    MuiPaper: {
      root: {
        boxShadow: "0 24px 24px 4px rgba(31,51,73,.1)!important",
        // boxShadow: "none !important"
        marginLeft: "8px",
        marginRight: "8px"
      }
    }
  }
});

function withRoot(Component) {
  function WithRoot(props) {
    //MuiThemeProvider makes the theme available down the React tree
    //CssBaseline kickstart an elegant, consistent, and simple baseline to build upon
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...props} />
      </MuiThemeProvider>
    );
  }
  return WithRoot;
}
export default withRoot;
