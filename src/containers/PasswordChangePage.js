//main
import React, { Component } from 'react';
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import {
  withRouter,
} from 'react-router-dom';
import AuthUserContext from '../components/authlogic/AuthUserContext';
import withAuthorization from '../components/authlogic/withAuthorization';

//components
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PasswordChangeForm from '../components/authui/PasswordChangeForm';
import SignOutLink from '../components/authui/SignOutLink';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    paddingTop: theme.spacing.unit * 2,
  },
  paper: {
    padding: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 1,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
  display: {
    paddingTop:theme.spacing.unit * 2,
  },
});

const authCondition = (authUser) => !!authUser;

class PasswordChangePage extends Component {

  render(){
    const { classes } = this.props;
    return(
      <AuthUserContext.Consumer>
        {authUser =>
          <div className={classes.root}>
            <Grid container className={classes.container} alignItems="stretch" direction="column" justify="center">
              <Grid item className={classes.item}>
                <Paper className={classes.paper}>
                  <Typography className={classes.display} variant="h4" gutterBottom>Change Password</Typography>
                  <Typography className={classes.caption} variant="body1" gutterBottom>for: {authUser.email}</Typography>
                  <PasswordChangeForm />
                </Paper>
              </Grid>
            </Grid>
            <Grid container className={classes.container} alignItems="center" direction="column" justify="center">
              <Grid item className={classes.item}>
                <SignOutLink />
              </Grid> 
            </Grid>   
          </div>
        }
      </AuthUserContext.Consumer>
    )
  }
}

export default compose(
  withAuthorization(authCondition),
  withRouter,
  withStyles(styles),
)(PasswordChangePage)