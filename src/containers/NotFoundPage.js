//main
import React, { Component } from 'react';

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
  },
  container: {
  },
  item: {
    paddingTop: theme.spacing.unit * 8,
  },
});

class NotFoundPage extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container className={classes.container} alignItems="center" direction="row" justify="center">  
          <Grid item className={classes.item}>
            <Typography className={classes.caption} variant="h4" gutterBottom>This page does not exist.</Typography>
          </Grid>
        </Grid>
      </div>
    )
    
  }
}

  export default withStyles(styles)(NotFoundPage)