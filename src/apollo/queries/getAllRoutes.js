import gql from 'graphql-tag'

export default  gql`
  query {
    routeCollection {
      items {
        sys{
          id
        }
        name
        description
        grade
        location {
          lat
          lon
        }
        rating
        type
        fadate
        mediaCollection {
          items {
              contentType
              fileName
              url
          }
        }
      }
    }
  }
`