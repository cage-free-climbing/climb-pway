//main
import React, { Component } from 'react';

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import * as routes from '../../routes';
import {
  Link,
} from 'react-router-dom';

//components
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
  },
  button: {
    margin: theme.spacing.unit,
    textTransform: 'none',
    fontWeight: 600,
  },
});

class PasswordForgetLink extends Component {
    render() {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Button component={Link} to={routes.PASSWORD_FORGET} color="primary" className={classes.button}>
            Forgot Password
          </Button>
        </div>
      )
    }
  }

export default withStyles(styles)(PasswordForgetLink)