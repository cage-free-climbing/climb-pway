//main
import React from "react";
import { compose } from "recompose";

//router
import { withRouter } from "react-router";

//styles
import { withStyles } from "@material-ui/core/styles";

//material components
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import MoreVertIcon from "@material-ui/icons/MoreVert";

const styles = theme => ({
  root: {
    //paddingTop: theme.spacing.unit * 7
  },
  appBar: {
    borderBottomLeftRadius: theme.spacing.unit * 0.5,
    borderBottomRightRadius: theme.spacing.unit * 0.5,
    backgroundColor: "#fff",
    boxShadow: "none !important"
  },
  toolBar: {
    alignItems: "center",
    justifyContent: "space-between"
  },
  title: {
    paddingLeft: theme.spacing.unit * 2
  },
  grow: {
    flexGrow: 1
  },
  show: {
    transform: "translateY(0)",
    transition: "transform .5s"
  },
  hide: {
    transform: "translateY(-110%)",
    transition: "transform .5s"
  }
});

class NavBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      shouldShow: null,
      backButton: false
    };

    this.lastScroll = null;

    this.handleScroll = this.handleScroll.bind(this);
    // Alternatively, you can throttle scroll events to avoid
    // updating the state too often. Here using lodash.
    // this.handleScroll = _.throttle(this.handleScroll.bind(this), 100);
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll, { passive: true });
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll(evt) {
    const lastScroll = window.scrollY;

    if (lastScroll === this.lastScroll) {
      return;
    }

    const shouldShow =
      this.lastScroll !== null ? lastScroll < this.lastScroll : null;

    if (shouldShow !== this.state.shouldShow) {
      this.setState((prevState, props) => ({
        ...prevState,
        shouldShow
      }));
    }

    this.lastScroll = lastScroll;
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar
          className={`${classes.appBar} ${
            this.state.shouldShow === null
              ? ""
              : this.state.shouldShow
                ? classes.show
                : classes.hide
          }`}
          position="fixed"
        >
          <Toolbar className={classes.toolBar} disableGutters>
            {this.state.backButton === true && (
              <IconButton aria-label="Back" onClick={this.goBack}>
                <ArrowBackIcon />
              </IconButton>
            )}
            <div className={classes.grow} />
            <IconButton aria-label="Map" color="default" onClick={this.goBack}>
              <MoreVertIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default compose(
  withRouter,
  withStyles(styles)
)(NavBar);
