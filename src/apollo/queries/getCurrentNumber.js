import gql from 'graphql-tag'

export default  gql`
  query {
      currentNumber @client {
        number
      }
  }
`