//main
import React, { Component } from 'react'
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import List from '@material-ui/core/List';
import Route from './Route'

const styles = theme => ({
  root: {
  },
})

class RouteList extends Component {
  render() {
    const { classes, routes} = this.props;
    return (
      <div className={classes.root}> 
        <List disablePadding>
          {routes.map(route =>
            <Route key={route.sys.id} route={route}/>
          )}
        </List>
      </div>
    )
  }
}

export default compose(
  withStyles(styles),
)(RouteList)
