import gql from 'graphql-tag'

export default  gql`
  mutation updateCurrentNumber($value:String!){
      updateCurrentNumber(value: $value) @client{
            number
      }
  }
`