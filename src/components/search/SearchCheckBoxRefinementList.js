//main
import React, { Component } from 'react';
import { compose } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import SearchCheckBoxItem from './SearchCheckBoxItem'

const styles = theme => ({
  root: {
  },
  checkBox: {
  },
  item: {
    padding: theme.spacing.unit * 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    height: '100%',
    color: theme.palette.text.primary,
    margin: 0,
    borderRadius: theme.spacing.unit * 2,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
});

class SearchCheckBoxRefinementList extends Component {
  
    render() {
      const { classes, items, attribute, refine, createURL, } = this.props;
      return (
        <div className={classes.root}>
          <List>
            <ListSubheader>{attribute}</ListSubheader>
            {items.map(item => (
              <SearchCheckBoxItem
                key={item.label}
                item={item}
                refine={refine}
                createURL={createURL}
              />
            ))}
        </List>
        </div>
      )
    }
  }

export default compose(
  withStyles(styles),
)(SearchCheckBoxRefinementList)