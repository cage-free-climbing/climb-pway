//main
import React, { Component } from 'react';
import { compose } from 'recompose';


//router
import withAuthorization from '../components/authlogic/withAuthorization';

//styles
import { withStyles } from '@material-ui/core/styles';
import SearchContent from '../components/search/SearchContent'

import {
  InstantSearch,
  Configure,
} from 'react-instantsearch-dom';

const styles = theme => ({
  root: {
  },
});

class SearchArea extends Component {
  render() {
    const { classes } = this.props;
    return(
      <div className={classes.root}>
        <InstantSearch
          appId="I0C0C3WQHX"
          apiKey="89daed94a8f0f43fd82a7c7c3a2199bb"
          indexName="contentful"
          searchState={this.props.searchState}
          createURL={this.props.createURL}
          onSearchStateChange={this.props.onSearchStateChange}
        >
          <Configure hitsPerPage={20} />
          <SearchContent />
        </InstantSearch>
      </div>
    )
  }
}

const authCondition = (authUser) => !!authUser;

export default compose(
  withAuthorization(authCondition),
  withStyles(styles),
)(SearchArea)