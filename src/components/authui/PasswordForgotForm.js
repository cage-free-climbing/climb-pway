//main
import React, { Component } from 'react';
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import * as routes from '../../routes';
import {
  Link,
  withRouter,
} from 'react-router-dom';

//auth
import { auth } from '../../firebase';

//components
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'
import SignInLink from './SignInLink'

const styles = theme => ({
  containerTop: {
    marginTop:theme.spacing.unit * 3,
  },
  containerBottom: {
    marginTop:theme.spacing.unit * 3,
    marginBottom:theme.spacing.unit * 3,
  },
  buttonLeft: {
    margin:-theme.spacing.unit * 3,
  },
  buttonright: {
    textTransform:'none',
    fontWeight: 600,
  },
});

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  error: null,
};

class PasswordForgetForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const { email } = this.state;

    auth.doPasswordReset(email)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });

    event.preventDefault();
  }

  render() {
    const { classes } = this.props;
    const {
      email,
      error,
    } = this.state;
    const isInvalid = email === '';

    return (
      <div className={classes.root}>
        <Grid container className={classes.containerTop} alignItems="stretch" direction="column" justify="center">
          <form noValidate autoComplete="off" onSubmit={this.onSubmit}>
            <Grid item className={classes.item}>
              <TextField
              fullWidth
              required
              id="required"
              label="Email"
              className={classes.textField}
              margin="normal"
              type="text"
              value={email}
              onChange={event => this.setState(byPropKey('email', event.target.value))}
              placeholder="Email Address"
              />
            </Grid>
            <Grid item className={classes.item}>
              { error && <p>{error.message}</p> }
            </Grid>
            <Grid container className={classes.containerBottom} alignItems="center" direction="row" justify="space-between">
              <Grid item className={classes.buttonLeft}>
                <SignInLink/>
              </Grid>
              <Grid item className={classes.item}> 
                <Button component={Link} to={routes.SIGN_IN} disabled={isInvalid} type="submit" variant="contained" color="primary" className={classes.buttonright}>Reset My Password</Button>
              </Grid>
            </Grid>
          </form>
        </Grid>   
      </div>

    );
  }
}

  export default compose(
    withRouter,
    withStyles(styles),
  )(PasswordForgetForm);