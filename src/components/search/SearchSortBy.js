//main
import React, { Component } from 'react';
import { compose } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import CloseIcon from '@material-ui/icons/Close';

const styles = theme => ({
  root: {
  },
  card: {
  },
  item: {
    padding: theme.spacing.unit * 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    height: '100%',
    color: theme.palette.text.primary,
    margin: 0,
    borderRadius: theme.spacing.unit * 2,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
});

class SearchSortBy extends Component {

  state = {
    anchorEl: null,
  };
  
  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes, items } = this.props;
    const { anchorEl } = this.state;

    return (
      <div className={classes.root}>
        <IconButton aria-owns={anchorEl ? 'simple-menu' : null} aria-haspopup="true" onClick={this.handleClick}>
          <CloseIcon style={{ marginTop: 13 }} />
        </IconButton>
        <Menu  id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}>
          {items.map(item => (
            <MenuItem
              key={item.value}
              value={item.value}
              onClick={() => {
                this.props.refine(item.value);
              }}
            >
              {item.label}
            </MenuItem>
            
            
          ))}
        </Menu>
      </div>
    )
  }
}

export default compose(
  withStyles(styles),
)(SearchSortBy)