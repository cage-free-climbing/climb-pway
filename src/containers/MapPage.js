import React, { Component } from 'react';
import { compose } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

import AuthUserContext from '../components/authlogic/AuthUserContext';
import withAuthorization from '../components/authlogic/withAuthorization';

//components
import MapView from '../components/map/MapView';


const styles = theme => ({
  root: {
  },
});


class MapPage extends Component {
  render() {
    const { classes } = this.props;

    return (
      <AuthUserContext.Consumer>
        {authUser =>
          <div className={classes.root}>
            <MapView />
          </div>
        }
      </AuthUserContext.Consumer>
      
    )
  }
}
const authCondition = (authUser) => !!authUser;

export default compose(
  withAuthorization(authCondition),
  withStyles(styles),
 ) (MapPage);