//main
import React, { Component } from 'react'
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

//root and styled card
const styles = theme => ({
  root: {
  },
  container: {
    height: 'calc(100%)',
  },
  appBar: {
    //position: 'relative',
    borderBottomLeftRadius: theme.spacing.unit*3,
    borderBottomRightRadius: theme.spacing.unit*3,
    backgroundColor: '#fff',
    paddingLeft: theme.spacing.unit*1,
    paddingRight: theme.spacing.unit*1,
  },
  flex: {
    flex: 1,
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  img: {
    maxWidth: '100%'
  },
  title: {
    fontWeight:600
  },
})

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class ImageList extends Component {
  state = {
    open: false,
    itemUrl: "",
    itemFileName: "",
  };

  handleClickOpen = (url, fileName) => {
    this.setState({ open: true, itemUrl: url, itemFileName: fileName });
  };

  handleClose = () => {
    this.setState({ open: false, itemUrl: "", });
  };

  render() {
    //setup classes from props
    const { classes, items } = this.props;
    const media = this.state

    //return card
    return (
      <div className={classes.root}>
      <GridList className={classes.gridList} cols={2.5}>
        {items.map(item => (
          <GridListTile onClick={() => this.handleClickOpen(item.url, item.fileName)} key={item.url}>
            <img src={item.url} alt={item.fileName} />
          </GridListTile>
        ))}
      </GridList>
      <Dialog
          fullScreen
          open={this.state.open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar}>
            <Toolbar disableGutters>
              <IconButton onClick={this.handleClose} aria-label="Close">
                <CloseIcon />
              </IconButton>
              <Typography variant="title" className={classes.title}>
                {media.itemFileName}
              </Typography>
            </Toolbar>
          </AppBar>
          <Grid container className={classes.container} alignItems="center" direction="column" justify="center">
            <Grid item className={classes.item}>
              <img className={classes.img} src={media.itemUrl} alt={media.itemFileName} />
            </Grid>
          </Grid>
        </Dialog>
      </div>

    )
  }
}

//return RouteList wrapped in withRoot, withStyles, graphql
export default compose(
  withStyles(styles),
)(ImageList)
