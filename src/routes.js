export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const PASSWORD_FORGET = '/pw-forget';
export const PASSWORD_CHANGE = '/pw-change';
export const ACCOUNT = '/account';
export const PAYMENT = '/payment';

export const LANDING = '/landing';
export const HOME = '/home';
export const MAP = '/map'
export const SEARCH = '/search'

export const ROUTE = '/route/:id';
export const AREA = '/area/:id';
