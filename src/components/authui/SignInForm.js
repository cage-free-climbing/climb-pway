//main
import React, { Component } from 'react';

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import { auth } from '../../firebase';
import * as routes from '../../routes';

//components
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'
import SignUpLink from './SignUpLink'

const styles = theme => ({
  containerTop: {
    marginTop:theme.spacing.unit * 3,
  },
  containerBottom: {
    marginTop:theme.spacing.unit * 3,
    marginBottom:theme.spacing.unit * 3,
  },
  buttonLeft: {
    margin:-theme.spacing.unit * 3,
  },
  buttonright: {
    textTransform:'none',
    fontWeight: 600,
  },
});

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

class SignInForm extends Component {
    constructor(props) {
      super(props);
      this.state = { ...INITIAL_STATE };
    }
  
    onSubmit = (event) => {
      const {
        email,
        password,
      } = this.state;
  
      const {
        history,
      } = this.props;
  
      auth.doSignInWithEmailAndPassword(email, password)
        .then(() => {
          this.setState(() => ({ ...INITIAL_STATE }));
          history.push(routes.HOME);
        })
        .catch(error => {
          this.setState(byPropKey('error', error));
        });
  
      event.preventDefault();
    }
  
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };
  
    render() {
  
      const { classes } = this.props;
  
      const {
        email,
        password,
        error,
      } = this.state;
  
      const isInvalid =
        password === '' ||
        email === '';
  
      return (
        <div className={classes.root}>
          <Grid container className={classes.containerTop} alignItems="stretch" direction="column" justify="center">
            <form noValidate autoComplete="off" onSubmit={this.onSubmit}>
              <Grid item className={classes.item}>
                <TextField
                  fullWidth
                  required
                  id="required"
                  label="Email"
                  className={classes.textField}
                  margin="normal"
                  type="text"
                  value={email}
                  onChange={event => this.setState(byPropKey('email', event.target.value))}
                  />
              </Grid>
              <Grid item className={classes.item}>
                <TextField
                  fullWidth
                  id="password-input"
                  label="Password"
                  className={classes.textField}
                  type="password"
                  autoComplete="current-password"
                  margin="normal"
                  value={password}
                  onChange={event => this.setState(byPropKey('password', event.target.value))}
                />
              </Grid>
              <Grid item className={classes.item}>
                { error && <p>{error.message}</p> }
              </Grid>
              <br/>
              <Grid container className={classes.containerBottom} alignItems="center" direction="row" justify="space-between">
                <Grid item className={classes.buttonLeft}>
                  <SignUpLink/>
                </Grid>
               <Grid item className={classes.item}> 
                  <Button disabled={isInvalid} type="submit" variant="contained" color="primary" className={classes.buttonright}>Sign In</Button>
                </Grid>
              </Grid>
            </form>
          </Grid>   
        </div>
      );
    }
  }

  export default withStyles(styles)(SignInForm);