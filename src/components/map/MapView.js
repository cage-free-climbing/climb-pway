//main
import React, { Component } from 'react';
import { compose } from 'recompose'

//data
import { Query } from "react-apollo";
import getAllRoutes from '../../apollo/queries/getAllRoutes'

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Loading from '../common/Loading';
import ApolloError from '../common/ApolloError';
import MapGL, {Marker, NavigationControl} from 'react-map-gl';
import ControlPanel from './ControlPanel';
import Pin from './Pin';
import User from './User';
import 'mapbox-gl/dist/mapbox-gl.css';
import MapCard from './MapCard';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import GpsFixedIcon from '@material-ui/icons/GpsFixed';

const TOKEN = 'pk.eyJ1IjoiYXJub2xkcGUwIiwiYSI6ImNqY3A1aGprdjJkZzAydm1yZ3pkbm1haTkifQ.et31REUJlsMFZPvegp447g';

const navStyle = {
  position: 'absolute',
  padding: '24px',
  display: 'flex',
};

const fabStyle = {
  position: 'absolute',
  padding: '24px',
  display: 'flex',
  bottom: '112px',
  right: '0px',
};

const styles = theme => ({
  root: {
    height: 300,
    width: 200,
  },
});


class MapView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        latitude: 0,
        longitude: 0,
      },
      viewport: {
        latitude: 43.101963,
        longitude: -71.177292,
        zoom: 13.7,
        width: 200,
        height: 200,
      },
      popupInfo: null,
      checked: false,
    };
  }

  handleChange = (route) => {
    this.setState(state => ({ checked: true }))
    this.setState({popupInfo: route})
  };

  componentDidMount() {
    window.addEventListener('resize', this._resize);
    this._resize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._resize);
  }

  _resize = () => {
    this.setState({
      viewport: {
        ...this.state.viewport,
        width: this.props.width || window.innerWidth,
        height: this.props.height || window.innerHeight
      }
    });
  };

  _updateViewport = (viewport) => {
    this.setState({viewport});
  }
  _updateUser = (user) => {
    this.setState({user});
  }

  _locateUser = (viewport) => {
    navigator.geolocation.getCurrentPosition(position => {
      this._updateViewport({
        ...this.state.viewport,
        longitude: position.coords.longitude,
        latitude: position.coords.latitude,
      });
      this._updateUser({
        ...this.state.user,
        longitude: position.coords.longitude,
        latitude: position.coords.latitude,
      });
    });
  }

  _renderUser = () => {
    return (
      <Marker key={`marker-user`}
        longitude={this.state.user.longitude}
        latitude={this.state.user.latitude}>
        <User size={24}/>
      </Marker>
    );
  }

  _mapClick = () => {
    this.setState({ checked: false })
  }

  _renderRoute = (route, index) => {
    return (
      <Marker key={`marker-${index}`}
        longitude={route.location.lon}
        latitude={route.location.lat} >
        <Pin size={24} onClick={() => this.handleChange(route)} />
      </Marker>
    );
  }

  render() {
    const { viewport, popupInfo, checked, user } = this.state;
    const { classes } = this.props;

    return (
      <Query query={getAllRoutes} errorPolicy="all">
        {({ loading, error, data }) => {
          if (loading) return <Loading/>
          if (error) return <ApolloError apolloError={error}/>
          return(
            <div className={classes.root}>
              <MapGL
                {...viewport}
                mapStyle="mapbox://styles/arnoldpe0/cjcp5p9cc3wof2rpos2ynja10"
                onViewportChange={this._updateViewport}
                mapboxApiAccessToken={TOKEN}
                onClick={this._mapClick}
              >
              <div className="nav" style={navStyle}>
                <NavigationControl onViewportChange={this._updateViewport} />
              </div>
              <div className="fab" style={fabStyle}>
              <Button className={classes.button} variant="fab" color="primary" aria-label="Locate User"  onClick={this._locateUser}>
                <GpsFixedIcon />
              </Button>
              </div>
              <Slide direction="up" in={checked} mountOnEnter unmountOnExit>
                <MapCard info={popupInfo} />
                </Slide>
                { data.routeCollection.items.map(this._renderRoute) }
                { this._renderUser() }
                <ControlPanel containerComponent={this.props.containerComponent} />
              </MapGL>
            </div>
          )
        }}
      </Query>
    );
  }
}

export default compose(
  withStyles(withStyles),
 ) (MapView);