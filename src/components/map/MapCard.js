//main
import React from 'react'
import { compose } from 'recompose';

//data
import { Mutation } from 'react-apollo';
import updateInfoWrapperTab from '../../apollo/mutations/updateInfoWrapperTab'

//router
import { Link } from 'react-router-dom';

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import StarRating from '../common/StarRating'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';


//root and styled card
const styles = theme => ({
  root: {
    position: 'absolute',
    bottom: 128,
    width:'100%',
  },
  card: {
    display: 'flex',
    marginLeft: theme.spacing.unit * 2,
    marginRight:theme.spacing.unit * 2,
  },
  content: {
    flex: '1 0 auto',
  },
  media: {
    width: 104,
    height: 104,
  },
  button: {
    textTransform: 'capitalize',
  },
  itemSpace: {
    paddingRight: theme.spacing.unit * 2,
  },
  paper: {
    borderRadius: theme.spacing.unit * 2,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
});

class MapCard extends React.Component {
  render(){
    const { classes, info } = this.props;
    return (
      <Mutation mutation={updateInfoWrapperTab}>
        {(updateInfoWrapperTab, { data }) => (
          <div className={classes.root}>
            <Card className={classes.card}>
              <CardMedia
                className={classes.media}
                image={info.mediaCollection.items[0].url}
                title={info.mediaCollection.items[0].fileName}
              />
              <CardContent className={classes.content}>
                <Typography gutterBottom variant="subheading" noWrap>{info.name}</Typography>
                <Grid className={classes.container}
                  container
                  alignItems="center"
                  direction="row"
                  justify="flex-start"
                >
                  <Grid className={classes.itemSpace} 
                        item
                  >
                    <Typography variant="caption" noWrap>V{info.grade}</Typography>
                  </Grid>
                  <Grid className={classes.itemSpace} 
                        item
                  >
                    <StarRating rating={info.rating}/>
                  </Grid>
                  <Grid className={classes.item} 
                        item
                  >
                    <Button className={classes.button} component={Link} to={`/route/${info.sys.id}`} onClick={(e, value) => {
                      updateInfoWrapperTab({ variables: { value: 0 } })
                    }}size="small" color="primary">More Info</Button>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </div>
        )}
      </Mutation>
    )
  }
}

export default compose(
  withStyles(styles),
)(MapCard)
