//main
import React, { Component } from 'react';
import { compose } from 'recompose';


//styles
import { withStyles } from '@material-ui/core/styles';

import {
  withRouter,
} from 'react-router-dom';

//components
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PasswordForgetForm from '../components/authui/PasswordForgotForm'
import SignUpLink from '../components/authui/SignUpLink';

const styles = theme => ({
  root: {
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    paddingTop: theme.spacing.unit * 2,
  },
  paper: {
    padding: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 1,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
  display: {
    paddingTop:theme.spacing.unit * 2,
  },
});

class PasswordForgetPage extends Component {
  render(){
    const { classes } = this.props;
    return(
    <div className={classes.root}>
        <Grid container className={classes.container} alignItems="stretch" direction="column" justify="center">
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography className={classes.display} variant="h4" gutterBottom>Forgot Password</Typography>
              <PasswordForgetForm />
            </Paper>
          </Grid>
        </Grid>
        <Grid container className={classes.container} alignItems="center" direction="column" justify="center">
        <Grid item className={classes.item}>
          <SignUpLink />
        </Grid> 
      </Grid>   
      </div>
    )
  }
}

export default compose(
  withRouter,
  withStyles(styles),
)(PasswordForgetPage)