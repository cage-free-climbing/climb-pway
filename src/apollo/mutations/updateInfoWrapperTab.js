import gql from 'graphql-tag'

export default  gql`
  mutation updateInfoWrapperTab($value:String!){
      updateInfoWrapperTab(value: $value) @client{
          tab
      }
  }
`