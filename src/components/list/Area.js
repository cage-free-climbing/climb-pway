//main
import React, { Component } from 'react'
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import { Link } from 'react-router-dom';

//components
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';

const styles = theme => ({
  root: {
  },
  firstTextItem: {
  },
});

class Area extends Component {
  render() {
    const { classes, area } = this.props;
    return (
      <div className={classes.root}>
        <ListItem disableGutters component={Link} to={`/area/${area.sys.id}`}>
          <Avatar src={area.mediaCollection.items[0].url} />
          <ListItemText className={classes.firstTextItem}
            primary={area.name}
            secondary={area.description}
            primaryTypographyProps={{noWrap: true}}
            secondaryTypographyProps={{noWrap: true}}
          />
        </ListItem>
      </div>
    )
  }
}

export default compose(
  withStyles(styles)
)(Area)
