//main
import React, { Component } from 'react';
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import * as routes from '../../routes';
import {
  withRouter,
} from 'react-router-dom';

//auth
import { auth, db } from '../../firebase';

//components
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'
import SignInLink from './SignInLink'

const styles = theme => ({
  containerTop: {
    marginTop:theme.spacing.unit * 3,
  },
  containerBottom: {
    marginTop:theme.spacing.unit * 3,
    marginBottom:theme.spacing.unit * 3,
  },
  buttonLeft: {
    margin:-theme.spacing.unit * 3,
  },
  buttonright: {
    textTransform:'none',
    fontWeight: 600,
  },
});

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

class SignUpForm extends Component {
    constructor(props) {
      super(props);
      this.state = { ...INITIAL_STATE };
    }
  
    onSubmit = (event) => {
      const {
        username,
        email,
        passwordOne,
      } = this.state;
  
      const {
        history,
      } = this.props;
  
      auth.doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        // Create a user in Firebase Database
        db.doCreateUser(authUser.user.uid, username, email)
          .then(() => {
            this.setState(() => ({ ...INITIAL_STATE }));
            history.push(routes.HOME);
          })
          .catch(error => {
            this.setState(byPropKey('error', error));
          });
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });
      event.preventDefault();
     }
  
  render() {
    const { classes } = this.props;
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      email === '' ||
      username === '';

    return (
      <div className={classes.root}>
          <Grid container className={classes.containerTop} alignItems="stretch" direction="column" justify="center">
            <form noValidate autoComplete="off" onSubmit={this.onSubmit}>
              <Grid item className={classes.item}>
                <TextField
                    fullWidth
                    required
                    id="required"
                    label="Full Name"
                    className={classes.textField}
                    margin="normal"
                    type="text"
                    value={username}
                    onChange={event => this.setState(byPropKey('username', event.target.value))}
                    placeholder="Full Name"
                    />
              </Grid>
              <Grid item className={classes.item}>
                <TextField
                  fullWidth
                  required
                  id="email-input"
                  label="Email"
                  className={classes.textField}
                  autoComplete="current-password"
                  margin="normal"
                  type="text"
                  value={email}
                  onChange={event => this.setState(byPropKey('email', event.target.value))}
                  placeholder="Email Address"
                />
                </Grid>
                <Grid item className={classes.item}>
                  <TextField
                    fullWidth
                    id="password-input"
                    label="Password"
                    className={classes.textField}
                    type="password"
                    autoComplete="current-password"
                    margin="normal"
                    value={passwordOne}
                    onChange={event => this.setState(byPropKey('passwordOne', event.target.value))}
                    placeholder="Password"
                  />
              </Grid>
              <Grid item className={classes.item}>
                  <TextField
                    fullWidth
                    id="password-input"
                    label="Password"
                    className={classes.textField}
                    type="password"
                    autoComplete="current-password"
                    margin="normal"
                    value={passwordTwo}
                    onChange={event => this.setState(byPropKey('passwordTwo', event.target.value))}
                    placeholder="Confirm Password"
                  />
              </Grid>
              <br/>
              <Grid container className={classes.containerBottom} alignItems="center" direction="row" justify="space-between">
                <Grid item className={classes.buttonLeft}>
                  <SignInLink/>
                </Grid>
                <Grid item className={classes.item}> 
                  <Button disabled={isInvalid} type="submit" variant="contained" color="primary" className={classes.buttonright}>Create Account</Button>
                </Grid>
            </Grid>
          { error && <p>{error.message}</p> }
          </form>
        </Grid>   
      </div>
    );
  }
}

  export default compose(
    withRouter,
    withStyles(styles),
  )(SignUpForm);