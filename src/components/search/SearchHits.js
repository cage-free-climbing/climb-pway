//main
import React, { Component } from 'react';
import { compose } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

//search
import { Highlight} from 'react-instantsearch-dom';

//router
import { Link } from 'react-router-dom';

//components
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';

const styles = theme => ({
  root: {
  },
  card: {
  },
  item: {
    padding: theme.spacing.unit * 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    height: '100%',
    color: theme.palette.text.primary,
    margin: 0,
    borderRadius: theme.spacing.unit * 2,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
});

class SearchHits extends Component {
  
    render() {
      const { classes, hits, marginLeft, hasMore, refine } = this.props;
      return (
        <div className={classes.root}>
          {hits.map(hit => (
            <ListItem key={hit.objectID} component={Link} to={`/${hit.contentType}/${hit.id}`} disableGutters>
            <Avatar src={hit.media[0].file.url}></Avatar>
            <ListItemText primary={<Highlight attribute="name" hit={hit}/>} secondary={hit.description} primaryTypographyProps={{noWrap: true}} secondaryTypographyProps={{noWrap: true}} />
          </ListItem>
          ))}
        </div>
      )
    }
  }

export default compose(
  withStyles(styles),
)(SearchHits)