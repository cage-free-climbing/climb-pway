//main
import React, { Component } from 'react';

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import * as routes from '../../routes';
import {
  Link,
} from 'react-router-dom';

//components
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
  },
  button: {
    textTransform: 'none',
    fontWeight: 600,
  },
  input: {
    display: 'none',
  },
});

class PaymentLink extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Button component={Link} to={routes.PAYMENT} variant="contained" color="primary" className={classes.button}>
          Payment
        </Button>
      </div>
    )
  }
}

  export default withStyles(styles)(PaymentLink)