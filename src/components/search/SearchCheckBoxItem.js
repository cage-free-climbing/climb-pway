//main
import React, { Component } from 'react';
import { compose } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Checkbox from '@material-ui/core/Checkbox';

const styles = theme => ({
  root: {
  },
  checkBox: {
  },
  item: {
    padding: theme.spacing.unit * 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    height: '100%',
    color: theme.palette.text.primary,
    margin: 0,
    borderRadius: theme.spacing.unit * 2,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
});

class SearchCheckBoxItem extends Component {
  
    render() {
      const { classes, item, refine, } = this.props;
      return (
        <div className={classes.root}>
          <Checkbox
            checked={item.isRefined}
            onChange={e => {
              e.preventDefault();
              refine(item.value);
            }}
          />
          {item.label}
        </div>
      )
    }
  }

export default compose(
  withStyles(styles),
)(SearchCheckBoxItem)