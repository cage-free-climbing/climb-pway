import { db } from "./firebase";

// User API

export const doCreateUser = (id, username, email) =>
  db.ref(`users/${id}`).set({
    username,
    email
  });

export const onceGetUsers = () => db.ref("users").once("value");

export const doCreatePaymentSource = (token, uid) =>
  db.ref(`/users/${uid}/sources`).push({
    token: token.id
  });

export const doCreatePaymentCharge = (token, uid) =>
  db.ref(`/users/${uid}/charges`).push({
    source: token,
    amount: parseInt("300"),
    currency: "usd",
    description: "Climb Pway"
  });
