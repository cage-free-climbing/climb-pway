//main
import React, { Component } from 'react';
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import * as routes from '../../routes';
import {
  withRouter,
  Link,
} from 'react-router-dom';

//auth
import { auth } from '../../firebase';

//components
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'

const styles = theme => ({
  root: {

  },
  containerTop: {
    marginTop:theme.spacing.unit * 3,
  },
  containerBottom: {
    marginTop:theme.spacing.unit * 3,
    marginBottom:theme.spacing.unit * 3,
  },
  buttonLeft: {
    margin:-theme.spacing.unit * 2,
    textTransform:'none',
    fontWeight: 600,
  },
  buttonRight: {
    textTransform:'none',
    fontWeight: 600,
  },
});

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

class PasswordChangeForm extends Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const { passwordOne } = this.state;

    const {
      history,
    } = this.props;

    auth.doPasswordUpdate(passwordOne)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(routes.ACCOUNT);
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });
    event.preventDefault();
  }

  render() {
    const { classes } = this.props;

    const {
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '';

    return (
      <div className={classes.root}>
        <Grid container className={classes.containerTop} alignItems="stretch" direction="column" justify="center">
          <form noValidate autoComplete="off" onSubmit={this.onSubmit}>
            <Grid item className={classes.item}>
              <TextField
                fullWidth
                id="password-input"
                label="Password"
                className={classes.textField}
                type="password"
                autoComplete="current-password"
                margin="normal"
                value={passwordOne}
                onChange={event => this.setState(byPropKey('passwordOne', event.target.value))}
              />
          </Grid>
          <Grid item className={classes.item}>
              <TextField
                fullWidth
                id="password-input"
                label="Confirm Password"
                className={classes.textField}
                type="password"
                autoComplete="current-password"
                margin="normal"
                value={passwordTwo}
                onChange={event => this.setState(byPropKey('passwordTwo', event.target.value))}
              />
          </Grid>
          <Grid container className={classes.containerBottom} alignItems="center" direction="row" justify="space-between">
            <Grid item className={classes.item}>
              <Button component={Link} to={routes.ACCOUNT} color="primary" className={classes.buttonLeft}>Cancel</Button>
            </Grid>
            <Grid item className={classes.item}> 
              <Button component={Link} to={routes.ACCOUNT} disabled={isInvalid} type="submit" variant="contained" color="primary" className={classes.buttonRight}>Change Password</Button>
            </Grid>
          </Grid>
          { error && <p>{error.message}</p> }
          </form>
        </Grid>   
      </div>
    )
  }
}

export default compose(
  withRouter,
  withStyles(styles),
)(PasswordChangeForm);