//main
import React, { Component } from "react";
import { compose } from "recompose";

//router
import withAuthorization from "../components/authlogic/withAuthorization";

//data
import { Query } from "react-apollo";
import getAllRoutesAllAreas from "../apollo/queries/getAllRoutesAllAreas";

//styles
import { withStyles } from "@material-ui/core/styles";

//components
import Loading from "../components/common/Loading";
import ApolloError from "../components/common/ApolloError";
import NavBar from "../components/navbar/NavBar";
import AreaList from "../components/list/AreaList";
import RouteList from "../components/list/RouteList";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
  root: {},
  title: {
    paddingTop: theme.spacing.unit * 7,
    display: "flex",
    justifyContent: "center"
  },
  item: {
    paddingTop: theme.spacing.unit * 1
  },
  caption: {
    // borderBottom: "1px solid rgba(0, 0, 0, .12)",
    // paddingBottom: theme.spacing.unit * 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 1
  }
});

class HomePage extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Query query={getAllRoutesAllAreas} errorPolicy="all">
        {({ loading, error, data }) => {
          if (loading) return <Loading />;
          if (error) return <ApolloError apolloError={error} />;
          return (
            <div className={classes.root}>
              <NavBar />
              <Grid
                container
                item
                className={classes.container}
                alignItems="stretch"
                direction="column"
                justify="flex-start"
              >
                <Grid item className={classes.item}>
                  <Typography
                    className={classes.title}
                    variant="h4"
                    gutterBottom
                  >
                    Climb Pway
                  </Typography>
                </Grid>
                <Grid item className={classes.item}>
                  <Paper className={classes.paper}>
                    <Typography
                      className={classes.caption}
                      variant="h6"
                      gutterBottom
                    >
                      Areas
                    </Typography>
                    <AreaList areas={data.areaCollection.items} />
                  </Paper>
                </Grid>
                <Grid item className={classes.item}>
                  <Paper className={classes.paper}>
                    <Typography
                      className={classes.caption}
                      variant="h6"
                      gutterBottom
                    >
                      Routes
                    </Typography>
                    <RouteList routes={data.routeCollection.items} />
                  </Paper>
                </Grid>
                <Grid item className={classes.item}>
                  <Paper className={classes.paper}>
                    <Typography
                      className={classes.caption}
                      variant="h6"
                      gutterBottom
                    >
                      Routes
                    </Typography>
                    <RouteList routes={data.routeCollection.items} />
                  </Paper>
                </Grid>
              </Grid>
            </div>
          );
        }}
      </Query>
    );
  }
}

const authCondition = authUser => !!authUser;

export default compose(
  withAuthorization(authCondition),
  withStyles(styles)
)(HomePage);
