//main
import React, { Component } from "react";
import { compose } from "recompose";

//styles
import { withStyles } from "@material-ui/core/styles";

//router
import AuthUserContext from "../components/authlogic/AuthUserContext";
import withAuthorization from "../components/authlogic/withAuthorization";

//firebase
import { db } from "../firebase";

//components
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import UserList from "../components/user/UserList";
import SignOutLink from "../components/authui/SignOutLink";
import PasswordChangeLink from "../components/authui/PasswordChangeLink";
import PaymentLink from "../components/authui/PaymentLink";

const styles = theme => ({
  root: {},
  button: {
    textTransform: "none"
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    paddingTop: theme.spacing.unit * 2
  },
  paper: {
    padding: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 3,
    boxShadow: "0 0 30px 6px rgba(31,51,73,.1)!important"
  },
  display: {
    paddingTop: theme.spacing.unit * 2
  }
});

class AccountPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: null
    };
  }

  componentDidMount() {
    db.onceGetUsers().then(snapshot =>
      this.setState(() => ({ users: snapshot.val() }))
    );
  }
  render() {
    const { users } = this.state;
    const { classes } = this.props;
    return (
      <AuthUserContext.Consumer>
        {authUser => (
          <div className={classes.root}>
            <Grid
              container
              className={classes.container}
              alignItems="stretch"
              direction="column"
              justify="center"
            >
              <Grid item className={classes.item}>
                <Paper className={classes.paper}>
                  <Typography
                    className={classes.display}
                    variant="h4"
                    gutterBottom
                  >
                    Account
                  </Typography>
                  <Typography
                    className={classes.body}
                    variant="body1"
                    gutterBottom
                  >
                    {authUser.email}
                  </Typography>
                  <br />
                  <Grid
                    container
                    className={classes.containerBottom}
                    alignItems="flex-start"
                    direction="row"
                    justify="space-between"
                  >
                    <Grid item className={classes.item}>
                      <PasswordChangeLink />
                    </Grid>
                    <Grid item className={classes.item}>
                      <SignOutLink />
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
            <Grid
              container
              className={classes.container}
              alignItems="stretch"
              direction="column"
              justify="center"
            >
              <Grid item className={classes.item}>
                <Paper className={classes.paper}>
                  <Typography
                    className={classes.display}
                    variant="h4"
                    gutterBottom
                  >
                    Subscription
                  </Typography>
                  <PaymentLink />
                </Paper>
              </Grid>
            </Grid>
          </div>
        )}
      </AuthUserContext.Consumer>
    );
  }
}

const authCondition = authUser => !!authUser;

export default compose(
  withAuthorization(authCondition),
  withStyles(styles)
)(AccountPage);
