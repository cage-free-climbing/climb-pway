//main
import React, { Component } from 'react'
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import List from '@material-ui/core/List';
import Area from './Area'

const styles = theme => ({
  root: {
  },
})

class AreaList extends Component {
  render() {
  const { classes, areas} = this.props;
    return (
      <div className={classes.root}>
        <List disablePadding>
          {areas.map(area =>
            <Area key={area.sys.id} area={area}/>
          )}
        </List>
      </div>
    )
  }
}

export default compose(
  withStyles(styles),
)(AreaList)
