//main
import React, { Component } from 'react';

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
  },
  container: {
  },
  item: {
    paddingTop: theme.spacing.unit * 8,
  },
});

class ApolloError extends Component {
  render() {
    const { classes, error } = this.props;
    if (error) {
      return (
        <div className={classes.root}>
          <Grid container className={classes.container} alignItems="center" direction="row" justify="center">
          <Grid item className={classes.item}>
            <Typography variant="body1" gutterBottom>Known Error</Typography>
            <Typography variant="body1" gutterBottom>{error.graphQLErrors.message}</Typography>
            {error.graphQLErrors.map(({ message }, i) => (
              <Typography variant="body1" gutterBottom key={i}>{message}</Typography>
            ))}
            </Grid>
          </Grid>
        </div>
      )
    } else {
      return (
        <div className={classes.root}>
          <Grid container className={classes.container} alignItems="center" direction="row" justify="center">  
            <Grid item className={classes.item}>
            <Typography variant="body1" gutterBottom>Unknown Error</Typography>
            </Grid>
          </Grid>
        </div>
      )
    }
    
  }
}

  export default withStyles(styles)(ApolloError)