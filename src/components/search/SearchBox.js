//main
import React, { Component } from 'react';
import { compose, renderComponent } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
root: {
},

});

class SearchBox extends React.Component{
render(){
        const { classes, currentRefinement, refine } = this.props
        return(
            <div className={classes.root}>
                <TextField
                    value={currentRefinement}
                    onChange={e => refine(e.target.value)}
                    id="SearchBox"
                    fullWidth
                    type="search"
                    placeholder="Search for routes..."
                />
            </div>
        )
    }
  }

  export default compose(
    withStyles(styles),
  )(SearchBox)