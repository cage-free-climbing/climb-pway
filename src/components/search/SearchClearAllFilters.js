//main
import React, { Component } from 'react';
import { compose } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
  },
  card: {
  },
  item: {
    padding: theme.spacing.unit * 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    height: '100%',
    color: theme.palette.text.primary,
    margin: 0,
    borderRadius: theme.spacing.unit * 2,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
});

class SearchClearAllFilters extends Component {
  
    render() {
      const { classes, items, refine } = this.props;
      return (
        <div className={classes.root}>
          <Button
            onClick={() => refine(items)}
            label="Clear All"
            style={{ height: 48, width: '100%' }}
            className="ClearAll"
          >
          CLEAR
          </Button>
        </div>
      )
    }
  }

export default compose(
  withStyles(styles),
)(SearchClearAllFilters)