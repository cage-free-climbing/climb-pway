//main
import React, { Fragment } from "react";
import { compose } from "recompose";

//code splitting
import Loadable from "react-loadable";

//router
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import * as routes from "./routes";
import withAuthentication from "./components/authlogic/withAuthentication";

//stripe
import { StripeProvider } from "react-stripe-elements";

//styles
import withRoot from "./components/themelogic/withRoot";
import { withStyles } from "@material-ui/core/styles";

//components
import Loading from "./components/common/Loading";

// async import
const LandingPage = Loadable({
  loader: () => import("./containers/LandingPage"),
  loading: Loading
});
const SignUpPage = Loadable({
  loader: () => import("./containers/SignUpPage"),
  loading: Loading
});
const SignInPage = Loadable({
  loader: () => import("./containers/SignInPage"),
  loading: Loading
});
const PasswordForgetPage = Loadable({
  loader: () => import("./containers/PasswordForgetPage"),
  loading: Loading
});
const PasswordChangePage = Loadable({
  loader: () => import("./containers/PasswordChangePage"),
  loading: Loading
});
const PaymentPage = Loadable({
  loader: () => import("./containers/PaymentPage"),
  loading: Loading
});
const MapPage = Loadable({
  loader: () => import("./containers/MapPage"),
  loading: Loading
});
const SearchPage = Loadable({
  loader: () => import("./containers/SearchPage"),
  loading: Loading
});
const HomePage = Loadable({
  loader: () => import("./containers/HomePage"),
  loading: Loading
});
const AccountPage = Loadable({
  loader: () => import("./containers/AccountPage"),
  loading: Loading
});
const RoutePage = Loadable({
  loader: () => import("./containers/RoutePage"),
  loading: Loading
});
const AreaPage = Loadable({
  loader: () => import("./containers/AreaPage"),
  loading: Loading
});
const NotFoundPage = Loadable({
  loader: () => import("./containers/NotFoundPage"),
  loading: Loading
});
const BottomBar = Loadable({
  loader: () => import("./components/bottombar/BottomBar"),
  loading: Loading
});

//styles
const styles = theme => ({
  switch: {
    paddingBottom: theme.spacing.unit * 7,
    backgroundColor: "#fff"
  }
});

//get stripe key
const stripeKey =
  process.env.NODE_ENV === "production"
    ? process.env.REACT_APP_PROD_STRIPE_KEY
    : process.env.REACT_APP_DEV_STRIPE_KEY;

class App extends React.Component {
  constructor() {
    super();
    this.state = { stripe: null };
  }
  componentDidMount() {
    if (window.Stripe) {
      this.setState({ stripe: window.Stripe(stripeKey) });
    } else {
      document.querySelector("#stripe-js").addEventListener("load", () => {
        // Create Stripe instance once Stripe.js loads
        this.setState({ stripe: window.Stripe({ stripeKey }) });
      });
    }
  }
  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <StripeProvider apiKey={stripeKey}>
          <Router>
            <Route
              render={({ location }) => (
                <Fragment>
                  <div className={classes.switch}>
                    <Switch location={location}>
                      <Route
                        exact
                        path="/"
                        render={() => <Redirect to={routes.HOME} />}
                      />
                      <Route
                        exact
                        path={routes.SIGN_UP}
                        component={SignUpPage}
                      />
                      <Route
                        exact
                        path={routes.SIGN_IN}
                        component={SignInPage}
                      />
                      <Route
                        exact
                        path={routes.PASSWORD_FORGET}
                        component={PasswordForgetPage}
                      />
                      <Route
                        exact
                        path={routes.PASSWORD_CHANGE}
                        component={PasswordChangePage}
                      />
                      <Route
                        exact
                        path={routes.PAYMENT}
                        component={PaymentPage}
                      />
                      <Route
                        exact
                        path={routes.LANDING}
                        component={LandingPage}
                      />
                      <Route exact path={routes.HOME} component={HomePage} />
                      <Route exact path={routes.MAP} component={MapPage} />
                      <Route
                        exact
                        path={routes.SEARCH}
                        component={SearchPage}
                      />
                      <Route
                        exact
                        path={routes.ACCOUNT}
                        component={AccountPage}
                      />
                      <Route exact path={routes.ROUTE} component={RoutePage} />
                      <Route exact path={routes.AREA} component={AreaPage} />
                      <Route component={NotFoundPage} />
                    </Switch>
                  </div>
                  <BottomBar />
                </Fragment>
              )}
            />
          </Router>
        </StripeProvider>
      </Fragment>
    );
  }
}

export default compose(
  withAuthentication,
  withRoot,
  withStyles(styles)
)(App);
