import gql from 'graphql-tag'

export default  gql`
  query ($id: String!) {
    route (id:$id){
      name
      description
      grade
      rating
      type
      fadate
      mediaCollection {
        items {
          contentType
          fileName
          url
        }
      }
    }
  }
`