import gql from 'graphql-tag'

export default  gql`
  query {
    routeCollection {
      items {
        sys{
          id
        }
        name
        description
        grade
        rating
        type
        fadate
        mediaCollection {
          items {
              contentType
              fileName
              url
          }
        }
      }
    }
    areaCollection {
      items {
        sys{
          id
        }
        name
        description
        mediaCollection {
          items {
              contentType
              fileName
              url
          }
        }
      }
    }
  }
`