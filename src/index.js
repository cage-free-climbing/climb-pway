//main
import React from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'

//data
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { withClientState } from 'apollo-link-state';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { persistCache } from 'apollo-cache-persist';
import { onError } from 'apollo-link-error';
import defaults from './apollo/stateLink/defaults';
import resolvers from './apollo/stateLink/resolvers';
import typeDefs from './apollo/stateLink/typeDefs';

//containers
import App from './App'

//setup cache
const cache = new InMemoryCache()

//offline access
persistCache({
  cache,
  storage: window.localStorage,
});

//setup contentful data link
const httpLink = new HttpLink({
  uri: 'https://cdn.contentful.com/spaces/0histolewwge/graphql/alpha',
  headers: {
    authorization: `Bearer ${
      process.env.REACT_APP_CONTENTFUL_TOKEN
    }`,
  },
})

//setup local state link
const stateLink = withClientState({
  resolvers, defaults, cache, typeDefs
});

//compose links
 const link = ApolloLink.from([
  onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.map(({ message, locations, path }) =>
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
        ),
      );
    if (networkError) console.log(`[Network error]: ${networkError}`);
  }),
  //called second
  stateLink,
  //called first
  httpLink,
])

//setup apollo client
const client = new ApolloClient({
  link,
  cache,
});

//render app
ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
  ,document.getElementById('root')
)

//register SW
registerServiceWorker();