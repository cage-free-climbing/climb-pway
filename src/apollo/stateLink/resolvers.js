
import gql from 'graphql-tag'

export default {
  Mutation: {
    updateCurrentNumber: (_, { value }, { cache }) => {
      const query = gql`
        query getCurrentNumber{
          currentNumber @client {
            __typename
            number
          }
        }
      `
      const previousState = cache.readQuery({ query })
      const data = {
        ...previousState,
        currentNumber:{
          ...previousState.currentNumber,
          number: value
        }
      }
      cache.writeData({ query, data})
    },
    updateInfoWrapperTab: (_, { value }, { cache }) => {
      const query = gql`
        query getInfoWrappertab{
          infoWrapperTab @client {
            __typename
            tab
          }
        }
      `
      const previousState = cache.readQuery({ query })
      const data = {
        ...previousState,
        infoWrapperTab:{
          ...previousState.infoWrapperTab,
          tab: value
        }
      }
      cache.writeData({ query, data})
    },
  },
};