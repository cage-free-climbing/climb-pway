//main
import React, { Component } from "react";
import { compose } from "recompose";

//router
import withAuthorization from "../authlogic/withAuthorization";

//styles
import { withStyles } from "@material-ui/core/styles";

//components
import ImageList from "../common/ImageList";
import RouteList from "../list/RouteList";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
  root: {},
  container: {
    paddingTop: theme.spacing.unit * 9
  },
  item: {
    paddingBottom: theme.spacing.unit * 1
  },
  caption: {
    // fontWeight: 600
  },
  paper: {
    padding: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 1,
    boxShadow: "0 0 30px 6px rgba(31,51,73,.1)!important"
  }
});

class InfoArea extends Component {
  render() {
    const { classes, data } = this.props;
    return (
      <div className={classes.root}>
        <Grid
          className={classes.container}
          container
          alignItems="stretch"
          direction="column"
          justify="flex-start"
        >
          <Grid className={classes.item} item>
            <Paper className={classes.paper}>
              <Typography variant="h6" gutterBottom>
                General
              </Typography>
              <Grid
                className={classes.containerSmall}
                container
                alignItems="flex-start"
                direction="row"
                justify="space-between"
              >
                <Grid className={classes.itemSmall} item>
                  <Typography variant="body1" gutterBottom>
                    7 Routes
                  </Typography>
                </Grid>
                <Grid className={classes.itemSmall} item>
                  <Typography variant="body1" gutterBottom>
                    V2-V8
                  </Typography>
                </Grid>
                <Grid className={classes.itemSmall} item>
                  <Typography variant="body1" gutterBottom>
                    Avg Rating 4.2
                  </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid className={classes.item} item>
            <Paper className={classes.paper}>
              <Typography variant="h6" gutterBottom>
                Media
              </Typography>
              <ImageList items={data.area.mediaCollection.items} />
            </Paper>
          </Grid>
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography variant="h6" gutterBottom>
                Description
              </Typography>
              <Typography variant="body1" gutterBottom>
                {data.area.description}
              </Typography>
            </Paper>
          </Grid>
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography variant="h6" gutterBottom>
                Routes
              </Typography>
              <RouteList routes={data.area.routesCollection.items} />
            </Paper>
          </Grid>
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography variant="h6" gutterBottom>
                Access
              </Typography>
              <Typography variant="body1" gutterBottom>
                {data.area.description}
              </Typography>
            </Paper>
          </Grid>
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography variant="h6" gutterBottom>
                Ethics
              </Typography>
              <Typography variant="body1" gutterBottom>
                {data.area.description}
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const authCondition = authUser => !!authUser;

export default compose(
  withAuthorization(authCondition),
  withStyles(styles)
)(InfoArea);
