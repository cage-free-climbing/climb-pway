//main
import React, { Component } from 'react';

//firebase
import { auth } from '../../firebase';

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
  },
  button: {
    textTransform: 'none',
    fontWeight: 600,
  },
});

class SignOutLink extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Button  onClick={auth.doSignOut} color="primary" className={classes.button}>
        Sign Out
        </Button>
      </div>
    )
  }
}

export default withStyles(styles)(SignOutLink)