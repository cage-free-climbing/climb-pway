//main
import React, { Component } from 'react';

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
  },
  container: {
  },
  item: {
    paddingTop: theme.spacing.unit * 8,
  },
});

class Loading extends Component {
  render() {
    const { classes, error } = this.props;
    if (error) {
      return (
        <div className={classes.root}>
          <Grid container className={classes.container} alignItems="center" direction="row" justify="center">  
            <Grid item className={classes.item}>
              <Typography className={classes.caption} variant="body1" gutterBottom>{error}</Typography>
            </Grid>
          </Grid>
        </div>
      )
    } else {
      return (
        <div className={classes.root}>
          <Grid container className={classes.container} alignItems="center" direction="row" justify="center">  
            <Grid item className={classes.item}>
              <CircularProgress className={classes.progress} />
            </Grid>
          </Grid>
        </div>
      )
    }
    
  }
}

  export default withStyles(styles)(Loading)