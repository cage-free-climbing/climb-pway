export default {
  currentNumber: {
    __typename: 'currentNumber',
    number: 0,
  },
  infoWrapperTab: {
    __typename: 'infoWrapperTab',
    tab: 0,
  }
}  