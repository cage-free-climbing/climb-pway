//main
import React, { Component } from 'react'
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';
import Star from '@material-ui/icons/Star';
import StarBorder from '@material-ui/icons/StarBorder';

//components

//root and styled card
const styles = theme => ({
  root: {
  },
  icon: {
    fontSize: 12,
  },
})

class StarRating extends Component {
  render() {
    //setup classes from props
    const { classes, rating } = this.props;

    //return card
    return (
      <span className={classes.root}>
        {{
          1: <span><Star className={classes.icon} /><StarBorder className={classes.icon} /><StarBorder className={classes.icon} /><StarBorder className={classes.icon} /></span>,
          2: <span><Star className={classes.icon} /><Star className={classes.icon} /><StarBorder className={classes.icon} /><StarBorder className={classes.icon} /></span>,
          3: <span><Star className={classes.icon} /><Star className={classes.icon} /><Star className={classes.icon} /><StarBorder className={classes.icon} /></span>,
          4: <span><Star className={classes.icon} /><Star className={classes.icon} /><Star className={classes.icon} /><Star className={classes.icon} /></span>,
        }[rating]}
      </span>

    )
  }
}

//return RouteList wrapped in withRoot, withStyles, graphql
export default compose(
  withStyles(styles),
)(StarRating)
