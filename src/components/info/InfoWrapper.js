//main
import React from "react";
import { compose } from "recompose";

//router
import { withRouter } from "react-router";

//data
import { Query, Mutation } from "react-apollo";
import getInfoWrapperTab from "../../apollo/queries/getInfoWrapperTab";
import updateInfoWrapperTab from "../../apollo/mutations/updateInfoWrapperTab";

//styles
import { withStyles } from "@material-ui/core/styles";

//components
import Loading from "../common/Loading";
import ApolloError from "../common/ApolloError";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

import MapView from "../map/MapView";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    paddingTop: theme.spacing.unit * 7
  },
  tabs: {
    paddingLeft: theme.spacing.unit * 1,
    paddingRight: theme.spacing.unit * 1
  },
  tab: {
    textTransform: "none",
    fontWeight: 600
  },
  flexItemRight: {
    marginLeft: "auto"
  },
  grow: {
    flexGrow: 1
  },
  appBar: {
    borderBottomLeftRadius: theme.spacing.unit * 0.5,
    borderBottomRightRadius: theme.spacing.unit * 0.5,
    backgroundColor: "#fff"
  },
  indicator: {
    height: 4,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4
  },
  backButtonItem: {
    paddingLeft: 4,
    paddingRight: 8
  }
});

class InfoWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.goBack = this.goBack.bind(this);
  }

  goBack() {
    this.props.history.goBack();
  }

  render() {
    const { classes, title, children } = this.props;
    return (
      <Query query={getInfoWrapperTab} errorPolicy="all">
        {({ loading, error, data: { infoWrapperTab } }) => {
          if (loading) return <Loading />;
          if (error) return <ApolloError apolloError={error} />;
          return (
            <Mutation mutation={updateInfoWrapperTab}>
              {updateInfoWrapperTab => (
                <div className={classes.root}>
                  <AppBar
                    className={classes.appBar}
                    position="fixed"
                    color="default"
                  >
                    <Toolbar disableGutters>
                      <IconButton aria-label="Back" onClick={this.goBack}>
                        <ArrowBackIcon />
                      </IconButton>
                      <div className={classes.grow} />

                      <IconButton
                        aria-label="Map"
                        color="default"
                        onClick={this.goBack}
                      >
                        <MoreVertIcon />
                      </IconButton>
                    </Toolbar>
                    <Tabs
                      classes={{ indicator: classes.indicator }}
                      className={classes.tabs}
                      value={infoWrapperTab.tab}
                      onChange={(e, value) => {
                        updateInfoWrapperTab({ variables: { value: value } });
                      }}
                    >
                      <Tab className={classes.tab} label="Information" />
                      <Tab className={classes.tab} label="Map" />
                      <Tab className={classes.tab} label="Comments" />
                    </Tabs>
                  </AppBar>
                  {infoWrapperTab.tab === 0 && children}
                  {infoWrapperTab.tab === 1 && <MapView />}
                </div>
              )}
            </Mutation>
          );
        }}
      </Query>
    );
  }
}

export default compose(
  withRouter,
  withStyles(styles)
)(InfoWrapper);
