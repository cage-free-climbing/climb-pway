import * as auth from './auth';
import * as firebase from './firebase';
import * as db from './db';


//expose auth and firebase to the rest of the app --- keeps sepperate keep secure
export {
  db,
  auth,
  firebase,
};