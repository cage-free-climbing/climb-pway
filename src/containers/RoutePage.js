//main
import React, { Component } from 'react';
import { compose } from 'recompose';

//router
import withAuthorization from '../components/authlogic/withAuthorization';

//data
import { Query } from "react-apollo";
import getRoute from '../apollo/queries/getRoute'

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Loading from '../components/common/Loading';
import ApolloError from '../components/common/ApolloError';
import InfoWrapper from '../components/info/InfoWrapper';
import InfoRoute from '../components/info/InfoRoute';

const styles = theme => ({
  root: {
  },
})

class RoutePage extends Component {
  render() {
    const id = this.props.match.params.id
    const { classes } = this.props;
    return(
      <Query query={getRoute} variables={{ id:id} }>
        {({ loading, error, data }) => {
          if (loading) return <Loading/>
          if (error) return <ApolloError apolloError={error}/>
          return(
            <div className={classes.root}>
              <InfoWrapper title={data.route.name}>
                <InfoRoute data={data} />
              </InfoWrapper>
            </div>
          )
        }}
      </Query>
    )
  }
}

const authCondition = (authUser) => !!authUser;

export default compose(
  withAuthorization(authCondition),
  withStyles(styles),
)(RoutePage)