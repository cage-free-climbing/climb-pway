//main
import React, { Component } from 'react';
import { compose } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';


const styles = theme => ({
  root: {
  },
  card: {
  },
  item: {
    padding: theme.spacing.unit * 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    height: '100%',
    color: theme.palette.text.primary,
    margin: 0,
    borderRadius: theme.spacing.unit * 2,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
});

class SearchNestedList extends Component {
  
    render() {
      const { classes, id, items, refine } = this.props;
      return (
        <div className={classes.root}>
          <List>
            <ListSubheader>{id}</ListSubheader>
          </List>
        </div>
      )
    }
  }

export default compose(
  withStyles(styles),
)(SearchNestedList)