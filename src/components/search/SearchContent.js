//main
import React, { Component } from "react";
import { compose } from "recompose";

//styles
import { withStyles } from "@material-ui/core/styles";

//search
import {
  connectSearchBox,
  connectInfiniteHits,
  connectRefinementList,
  connectCurrentRefinements,
  connectHierarchicalMenu,
  connectSortBy
} from "react-instantsearch-dom";

//components
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import SearchBox from "./SearchBox";
import SearchHits from "./SearchHits";
import SearchCheckBoxRefinementList from "./SearchCheckBoxRefinementList";
import SearchClearAllFilters from "./SearchClearAllFilters";
import SearchNestedList from "./SearchNestedList";
import SearchSortBy from "./SearchSortBy";

const styles = theme => ({
  root: {},
  container: {},
  item: {
    paddingTop: theme.spacing.unit * 1
  },
  caption: {
    // fontWeight: 600
  },
  appBar: {
    borderBottomLeftRadius: theme.spacing.unit * 3,
    borderBottomRightRadius: theme.spacing.unit * 3,
    backgroundColor: "#fff",
    paddingLeft: theme.spacing.unit * 1,
    paddingRight: theme.spacing.unit * 1
  },
  paper: {
    padding: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 1,
    boxShadow: "0 0 30px 6px rgba(31,51,73,.1)!important"
  }
});

class Content extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar className={classes.appBar} position="static" color="default">
          <Toolbar>
            <ConnectedSearchBox />
          </Toolbar>
        </AppBar>
        <Grid
          container
          className={classes.container}
          alignItems="stretch"
          direction="column"
          justify="flex-start"
        >
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.caption}
                variant="caption"
                gutterBottom
                color="textPrimary"
              >
                Results
              </Typography>
              <ConnectedHits />
            </Paper>
          </Grid>
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.caption}
                variant="caption"
                gutterBottom
                color="textPrimary"
              >
                Sort By
              </Typography>
              <ConnectedSortBy
                items={[{ value: "contentful", label: "Indice 1" }]}
                defaultRefinement="contentful"
              />
            </Paper>
          </Grid>
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.caption}
                variant="caption"
                gutterBottom
                color="textPrimary"
              >
                Clear
              </Typography>
              <ConnectedCurrentRefinements />
            </Paper>
          </Grid>

          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.caption}
                variant="caption"
                gutterBottom
                color="textPrimary"
              >
                Check Box Refinement
              </Typography>
              <ConnectedCheckBoxRefinementList
                attribute="contentType"
                operator="or"
              />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const ConnectedNestedList = connectHierarchicalMenu(SearchNestedList);
const ConnectedSearchBox = connectSearchBox(SearchBox);
const ConnectedHits = connectInfiniteHits(SearchHits);
const ConnectedCurrentRefinements = connectCurrentRefinements(
  SearchClearAllFilters
);
const ConnectedCheckBoxRefinementList = connectRefinementList(
  SearchCheckBoxRefinementList
);
const ConnectedSortBy = connectSortBy(SearchSortBy);

export default compose(withStyles(styles))(Content);
