//main
import React, { Component } from 'react';
import { compose } from 'recompose'

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import { withRouter } from 'react-router-dom';

//components
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PasswordForgetLink from '../components/authui/PasswordForgetLink';
import SignInForm from '../components/authui/SignInForm'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    paddingTop: theme.spacing.unit * 2,
  },
  paper: {
    padding: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 1,
    boxShadow: '0 0 30px 6px rgba(31,51,73,.1)!important',
  },
  display: {
    paddingTop:theme.spacing.unit * 2,
  },
});

class SignInPage extends Component {
  render() {
    const { classes, history } = this.props;
    return(
      <div className={classes.root}>
        <Grid container className={classes.container} alignItems="stretch" direction="column" justify="center">
          <Grid item className={classes.item}>
            <Paper className={classes.paper}>
              <Typography className={classes.display} variant="h4" gutterBottom>Sign In</Typography>
              <SignInForm history={history} />
            </Paper>
          </Grid>
        </Grid> 
        <Grid container className={classes.container} alignItems="center" direction="column" justify="center">
          <Grid item className={classes.item}>
            <PasswordForgetLink />
          </Grid> 
        </Grid>   
      </div>
    )
  }
}
export default compose(
  withRouter,
  withStyles(styles),
)(SignInPage);
