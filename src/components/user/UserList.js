//main
import React, { Component } from 'react';
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//components
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  body:{
    
  }
});

class UserList extends Component {
  render(){
    const { users } = this.props;
    const { classes } = this.props;
    return(
      <div className={classes.root}>
        {Object.keys(users).map(key =>
          <div key={key}><Typography className={classes.body} variant="body1" gutterBottom>{users[key].username}</Typography></div>
        )}
      </div>
    )
  }
}

export default compose(
  withStyles(styles),
)(UserList)