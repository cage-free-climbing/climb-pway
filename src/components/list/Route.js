//main
import React, { Component } from 'react'
import { compose } from 'recompose';

//styles
import { withStyles } from '@material-ui/core/styles';

//router
import { Link } from 'react-router-dom';

//components
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import StarRating from '../common/StarRating'

const styles = theme => ({
  root: {
  },
  firstTextItem: {
  },
  secondTextItem: {
    minWidth: 48,
    padding: 0,
  },
});

class Route extends Component {
  render() {
    const { classes, route } = this.props;
    return (
      <div className={classes.root}>
        <ListItem disableGutters component={Link} to={`/route/${route.sys.id}`}> 
          <Avatar src={route.mediaCollection.items[0].url} />
          <ListItemText className={classes.firstTextItem}
            primary={route.name}
            secondary={route.description}
            primaryTypographyProps={{noWrap: true}}
            secondaryTypographyProps={{noWrap: true}}
          />
          <ListItemText className={classes.secondTextItem}
            primary={`V${route.grade}`}
            secondary={<StarRating rating={route.rating}/>}
            primaryTypographyProps={{noWrap: true, align: 'right'}}
            secondaryTypographyProps={{noWrap: true, align: 'right'}}
          />
        </ListItem>
      </div>
    )
  }
}

export default compose(
  withStyles(styles)
)(Route)
