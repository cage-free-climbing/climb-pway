//main
import React, { Component } from "react";
import { compose } from "recompose";

//router
import { Link } from "react-router-dom";
import * as routes from "../../routes";
import AuthUserContext from "../authlogic/AuthUserContext";

//data
import { Query, Mutation } from "react-apollo";
import getCurrentNumber from "../../apollo/queries/getCurrentNumber";
import updateCurrentNumber from "../../apollo/mutations/updateCurrentNumber";

//styles
import { withStyles } from "@material-ui/core/styles";

//components
import Loading from "../common/Loading";
import ApolloError from "../common/ApolloError";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import List from "@material-ui/icons/List";
import Search from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";

const styles = theme => ({
  root: {},
  bottomNavigation: {
    width: "-webkit-fill-available",
    position: "fixed",
    bottom: 0,
    borderTopLeftRadius: theme.spacing.unit * .5,
    borderTopRightRadius: theme.spacing.unit * .5,
    zIndex: 1100,
    boxShadow: "0 0 30px 6px rgba(31,51,73,.1)!important"
  }
});

class BottomBar extends Component {
  render() {
    const { classes } = this.props;
    return (
      <AuthUserContext.Consumer>
        {authUser =>
          authUser ? (
            <div className={classes.root}>
              <Query query={getCurrentNumber} errorPolicy="all">
                {({ loading, error, data: { currentNumber } }) => {
                  if (loading) return <Loading />;
                  if (error) return <ApolloError apolloError={error} />;
                  return (
                    <Mutation mutation={updateCurrentNumber}>
                      {(updateCurrentNumber, { data }) => (
                        <BottomNavigation
                          value={currentNumber.number}
                          onChange={(e, value) => {
                            updateCurrentNumber({
                              variables: { value: value }
                            });
                          }}
                          showLabels
                          className={classes.bottomNavigation}
                        >
                          <BottomNavigationAction
                            component={Link}
                            to={routes.HOME}
                            label="Explore"
                            icon={<List />}
                          />
                          <BottomNavigationAction
                            component={Link}
                            to={routes.SEARCH}
                            label="Search"
                            icon={<Search />}
                          />
                          <BottomNavigationAction
                            component={Link}
                            to={routes.ACCOUNT}
                            label="Account"
                            icon={<AccountCircle />}
                          />
                        </BottomNavigation>
                      )}
                    </Mutation>
                  );
                }}
              </Query>
            </div>
          ) : (
            <div className={classes.root} />
          )
        }
      </AuthUserContext.Consumer>
    );
  }
}

export default compose(withStyles(styles))(BottomBar);
