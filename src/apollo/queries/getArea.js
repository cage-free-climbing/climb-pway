import gql from 'graphql-tag'

export default  gql`
  query ($id: String!) {
    area(id: $id) {
      name
      description
      mediaCollection {
        items {
          contentType
          fileName
          url
        }
      }
      routesCollection {
        items {
          sys {
            id
          }
          name
          description
          grade
          rating
          mediaCollection {
            items {
              contentType
              fileName
              url
            }
          }
        }
      }
    }
  }
`