//main
import React, { Component } from "react";
import { compose } from "recompose";

//styles
import { withStyles } from "@material-ui/core/styles";

//router
import { withRouter } from "react-router-dom";
import AuthUserContext from "../components/authlogic/AuthUserContext";
import withAuthorization from "../components/authlogic/withAuthorization";

//components
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import PaymentForm from "../components/payment/PaymentForm";
import { Elements } from "react-stripe-elements";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    paddingTop: theme.spacing.unit * 2
  },
  item: {
    paddingTop: theme.spacing.unit * 1
  },
  paper: {
    padding: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 1,
    boxShadow: "0 0 30px 6px rgba(31,51,73,.1)!important"
  },
  caption: {
    // fontWeight: 600
  },
  display: {
    paddingTop: theme.spacing.unit * 2
  }
});

class PaymentPage extends Component {
  render() {
    const { classes } = this.props;
    return (
      <AuthUserContext.Consumer>
        {authUser => (
          <div className={classes.root}>
            <Grid
              container
              className={classes.container}
              alignItems="stretch"
              direction="column"
              justify="center"
            >
              <Grid item className={classes.item}>
                <Paper className={classes.paper}>
                  <Typography
                    className={classes.display}
                    variant="h4"
                    gutterBottom
                  >
                    Payment
                  </Typography>
                  <Typography
                    className={classes.body1}
                    variant="body1"
                    gutterBottom
                  >
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Pellentesque hendrerit lectus vitae mattis cursus. Nunc
                    vestibulum eros ut bibendum aliquam. Mauris non lacinia dui,
                    in scelerisque urna.
                  </Typography>
                </Paper>
              </Grid>
              <Grid item className={classes.item}>
                <Paper className={classes.paper}>
                  <Typography
                    className={classes.caption}
                    variant="caption"
                    gutterBottom
                  >
                    Subscribe
                  </Typography>
                  <Typography
                    className={classes.body1}
                    variant="body1"
                    gutterBottom
                  >
                    $3/month
                    <br />
                    $1 => Cage Free Climbing
                    <br />
                    $1 =>Content Author
                    <br />
                    $1 => Conservation Group
                    <br />
                  </Typography>
                  <Elements>
                    <PaymentForm authUser={authUser} />
                  </Elements>
                </Paper>
              </Grid>
            </Grid>
          </div>
        )}
      </AuthUserContext.Consumer>
    );
  }
}

const authCondition = authUser => !!authUser;

export default compose(
  withAuthorization(authCondition),
  withRouter,
  withStyles(styles)
)(PaymentPage);
